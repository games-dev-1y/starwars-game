# README #

This is a text-based StarWars themed console game, programmed in Assembly with Easy68k IDE.

### What is this repository for? ###

* Project - Develop a StarWars themed ship management console game

### How do I get set up? ###

* Clone repository
* install [Easy68k](http://www.easy68k.com/)

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)